import CryptoJS from './crypto-js/crypto-js.js'

/*
    DES(Data Encryption Standard)和TripleDES是对称加密的两种实现。
    DES和TripleDES基本算法一致，只是TripleDES算法提供的key位数更多，加密可靠性更高。
    DES使用的密钥key为8字节，初始向量IV也是8字节。
    TripleDES使用24字节的key，初始向量IV也是8字节。
*/
//des加密 DES-EDE3-CBC
//des加密 DES-EDE3-CBC
function encrypt(str,key) {
    key = CryptoJS.enc.Utf8.parse(key?key:"@yj4w0zBav4OteKa");// 秘钥
    var iv= CryptoJS.enc.Utf8.parse('1234567890123412');//向量iv
    var encrypted = CryptoJS.AES.encrypt(str, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.ZeroPadding });
    return encrypted.toString();
}
/**
 * 解密
 * @param str
 */
function decrypt(str) {
    var key = CryptoJS.enc.Utf8.parse("@yj4w0zBav4OteKa");// 秘钥
    var iv=    CryptoJS.enc.Utf8.parse('1234567890123412');//向量iv
    var decrypted = CryptoJS.AES.decrypt(str,key,{iv:iv,padding:CryptoJS.pad.ZeroPadding});
    return decrypted.toString(CryptoJS.enc.Utf8);
}
module.exports = {
    encrypt: encrypt,
    decrypt: decrypt
}